from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5f.txt"
WRITE_PATH = "Answers/rosalind_ba5f_ans.txt"

def ScoringMatrix():
	lines = ReadFromFile("Data/PAM250.txt")
	variables = lines[0].split()
	values = [line.split()[1:] for line in lines[1:]]

	scoring_matrix = {}
	for x in variables:
		for y in variables:
			scoring_matrix[(x,y)] = int(values[variables.index(x)][variables.index(y)])
	return scoring_matrix

def InitialMatrix(v,w):
	return [[0 for j in xrange(w)] for i in xrange(v)]

def LocalAlignment(v, w, sigma):
	# initialize an empty matrix
	matrix = InitialMatrix(len(v)+1, len(w)+1)
	back_track = InitialMatrix(len(v)+1,len(w)+1)


	# fill in the matrix with the scores
	scoring_matrix = ScoringMatrix()

	ix,jx = 0,0
	max_score_index = -float("inf")
	for i in xrange(1,len(v) + 1):
		for j in xrange(1,len(w)+1):
			# down, right, match
			# down if deletion, right if insert, and match is match
			# include the concept of free ride, cost 0 to achieve local alignment
			scores = [matrix[i-1][j] - sigma, matrix[i][j-1] - sigma, matrix[i-1][j-1] + scoring_matrix[v[i-1],w[j-1]],0]
			matrix[i][j] = max(scores)
			back_track[i][j] = scores.index(matrix[i][j])
			#record the index of i,j
			if max(scores) > max_score_index:
				ix, jx = i,j
				max_score_index = max(scores)

	# Short function to handle insertions and indeletions
	# it's an insertions if dash in top string
	# it's an deletion if dash in bottom string
	indel_insert = lambda e,i: e[:i] + "-" + e[i:]


	max_score = matrix[ix][jx]
	i,j = ix,jx
	v_aligned, w_aligned = v[:i],w[:j]

	# back track to where the alignment begins 
	# to give us the back score
	while i*j != 0 and back_track[i][j] != 3:
		if back_track[i][j] == 0:
			i -= 1
			w_aligned = indel_insert(w_aligned, j)
		elif back_track[i][j] == 1:
			j -= 1
			v_aligned = indel_insert(v_aligned, i)
		elif back_track[i][j] == 2:
			i -= 1
			j -= 1

	v_aligned = v_aligned[i:]
	w_aligned = w_aligned[j:]

	return max_score, v_aligned, w_aligned

lines = ReadFromFile(READ_PATH)
v = lines[0]
w = lines[1]

score, v, w = LocalAlignment(v,w,5)

f = open(WRITE_PATH, "w")
f.write(str(score)+"\n")
f.write(v+"\n")
f.write(w)
f.close()