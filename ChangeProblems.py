from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5a.txt"
WRITE_PATH = "Answers/rosalind_ba5a_ans.txt"

def ChangeProblem(money, Coins):
	MinCoins = {}
	MinCoins[0] = 0
	for m in xrange(1, money+1):
		MinCoins[m] = float("inf")
		for i in xrange(1, len(Coins)):
			coin = Coins[i]
			if m >= coin:
				if MinCoins[m - coin] + 1 < MinCoins[m]:
					MinCoins[m] = MinCoins[m - coin] + 1
	return MinCoins[money]

# take in the arguments from our file
lines = ReadFromFile(READ_PATH)
m = int(lines[0])
coins = [int(x) for x in lines[1].split(',')]

print ChangeProblem(m, coins)