from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5b.txt"
WRITE_PATH = "Answers/rosalind_ba5b_ans.txt"

def BuildMatrix(lines):
	Matrix = []
	for i in xrange(0, len(lines)):
		Matrix.append([])
		Matrix[i] = [int(x) for x in lines[i].split()]
	return Matrix

def InitialMatrix(n,m):
	Matrix = []
	for i in xrange(0,n):
		Matrix.append([])
		Matrix[i] = [0 for x in xrange(0,m)]
	return Matrix

def LongestPath(n,m,Down,Right):
	matrix = InitialMatrix(n+1,m+1)
	matrix[0][0] = 0

	for i in xrange(0,n):
		matrix[i+1][0] = matrix[i][0] + Down[i][0]
	for j in xrange(0,m):
		matrix[0][j+1] = matrix[0][j] + Right[0][j]
	for i in xrange(1,n+1):
		for j in xrange(1,m+1):
			r = Right[i][j-1]
			d = Down[i-1][j]
			matrix[i][j] = max([matrix[i-1][j]+d,matrix[i][j-1]+r])
	print matrix[n][m]

# parse the arguments from our read file
lines = ReadFromFile(READ_PATH)
n = int(lines[0].split()[0])
m = int(lines[0].split()[1])
first_matrix = BuildMatrix(lines[1:n+1])
second_matrix =  BuildMatrix(lines[n+2:])

LongestPath(n,m,first_matrix,second_matrix)
