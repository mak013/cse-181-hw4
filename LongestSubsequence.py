from helpers import ReadFromFile, InitialMatrix

READ_PATH = "Problems/rosalind_ba5c.txt"
WRITE_PATH = "Answers/rosalind_ba5c_ans.txt"

def LongestSubsequence(v,w):
	matrix = InitialMatrix(len(v) + 1, len(w) + 1)
	# form the scoring matrix 
	for i in xrange(len(v)):
		for j in xrange(len(w)):
			if v[i] == w[j]:
				matrix[i+1][j+1] = matrix[i][j] + 1
			else:
				matrix[i+1][j+1] = max([matrix[i+1][j], matrix[i][j+1]])
	# recover the longest common sequence string
	lcs = ""
	i = len(v)
	j = len(w)
	while i*j != 0:
		if matrix[i][j] == matrix[i-1][j]:
			i -= 1
		elif matrix[i][j] == matrix[i][j-1]:
			j -= 1
		else:
			lcs = v[i-1] + lcs
			i -= 1
			j -= 1
	return lcs
lines = ReadFromFile(READ_PATH)
v = lines[0]
w = lines[1]

lcs = LongestSubsequence(v,w)

f = open(WRITE_PATH, "w")
f.write(lcs)
f.close()