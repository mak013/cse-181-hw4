from helpers import ReadFromFile, InitialMatrix

READ_PATH = "Problems/rosalind_ba5h.txt"
WRITE_PATH = "Answers/rosalind_ba5h_ans.txt"

def FittingAlignment(v,w):
  # initialize our data structures
  score_matrix = InitialMatrix(len(v)+1,len(w)+1)
  back_track = InitialMatrix(len(v)+1,len(w)+1)

  # fill the score matrix and backtrack matrix
  for i in xrange(1, len(v)+1):
    for j in xrange(1,len(w)+1):
      # sets of possibles scores from the moves we can make
      # we include a penalty or +1 if they match or not
      scores = [score_matrix[i-1][j]-1, score_matrix[i][j-1]-1, score_matrix[i-1][j-1]+[-1,1][v[i-1]==w[j-1]]]
      score_matrix[i][j] = max(scores)
      back_track[i][j] = scores.index(score_matrix[i][j])
  # we start backtracking our score from the length of the shortest word for j
  j = len(w)
  # and i will be where the rows at index j in that row has the highest score
  i = max(enumerate([score_matrix[x][j] for x in xrange(len(w), len(v))]),key=lambda x: x[1])[0] + j

  # max score
  max_score = score_matrix[i][j]

  indel_insert = lambda x,i: x[:i] + "-" + x[i:]

  # reconstruct the string from our back track matrix
  reconstruct_v = v[:i]
  reconstruct_w = w[:j]


  # if 0, put a dash in the lower string
  # if 1, put a dash in the upper string
  # otherwise, do nothing
  while i*j != 0:
    if back_track[i][j] == 0:
      reconstruct_w = indel_insert(reconstruct_w, j)
      i -= 1
    elif back_track[i][j] == 1:
      reconstruct_v = indel_insert(reconstruct_v, i)
      j -= 1
    else:
      i-=1      
      j-=1

  # reconstruct the string
  reconstruct_v = reconstruct_v[i:]
  reconstruct_w = reconstruct_w[j:]

  return max_score, reconstruct_v, reconstruct_w

lines = ReadFromFile(READ_PATH)
v = lines[0]
w = lines[1]
score, newV, newW = FittingAlignment(v,w)

f = open(WRITE_PATH, "w")
f.write(str(score)+"\n")
f.write(newV+"\n")
f.write(newW)
f.close()