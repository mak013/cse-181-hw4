from helpers import ScoringMatrix, MiddleColumn, ReadFromFile

READ_PATH = "Problems/rosalind_ba5k.txt"
WRITE_PATH = "Answers/rosalind_ba5k_ans.txt"

def MiddleEdge(v, w, scoring_matrix, sigma):
    source2middle = MiddleColumn(v, w, scoring_matrix, sigma)[0]
    middle2sink, backtrack = map(lambda l: l[::-1], MiddleColumn(v[::-1], w[::-1]+['', '$'][len(w) % 2 == 1 and len(w) > 1], scoring_matrix, sigma))

    scores = map(sum, zip(source2middle, middle2sink))
    max_middle = max(xrange(len(scores)), key=lambda i: scores[i])

    if max_middle == len(scores) - 1:
        next_node = (max_middle, len(w)/2 + 1)
    else:
        next_node = [(max_middle + 1, len(w)/2 + 1), (max_middle, len(w)/2 + 1), (max_middle + 1, len(w)/2),][backtrack[max_middle]]

    return (max_middle, len(w)/2), next_node

lines = ReadFromFile(READ_PATH)
v = lines[0]
w = lines[1]

ans1, ans2 = MiddleEdge(v,w, ScoringMatrix("Data/PAM250.txt"),5)
print ans1
print ans2