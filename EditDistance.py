from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5g.txt"
WRITE_PATH = "Answers/rosalind_ba5g_ans.txt"

def InitialMatrix(v,w):
	return [[0 for j in xrange(w)] for i in xrange(v)]

def EditDistance(v,w):
	# initiate the matrix
	matrix = InitialMatrix(len(v)+1,len(w)+1)
	# initiate the first row and first column of the matrix 
	# with the index of the the letter in the word as the score
	# for example, at 3,0 you need 3 delete operations to match nothing
	for i in xrange(1,len(v)+1):
		matrix[i][0] = i
	for j in xrange(1,len(w)+1):
		matrix[0][j] = j
	# fill the score matrix with edit distances
	# i,j represent how many edits you need to get j letters 
	# to look like i letters
	for i in xrange(1,len(v)+1):
		for j in xrange(1,len(w)+1):
			if v[i-1] == w[j-1]:
				matrix[i][j] = matrix[i-1][j-1]
			else:
				scores = [matrix[i-1][j]+1,matrix[i][j-1]+1,matrix[i-1][j-1]+1]
				matrix[i][j] = min(scores)

	return matrix[len(v)][len(w)]

lines = ReadFromFile(READ_PATH)
v = lines[0]
w = lines[1]

print EditDistance(v,w)