from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5d.txt"
WRITE_PATH = "Answers/rosalind_ba5d_ans.txt"

def FormGraph(lines):
    graph = {}
    for line in lines:
        nodeA = int(line.split("->")[0])
        nodeB = int(line.split("->")[1].split(":")[0])
        weight = int(line.split("->")[1].split(":")[1])
        graph[(nodeA,nodeB)] = weight
    return graph

def TopologicalOrder(graph, source):
    '''Returns a topological ordering for the given graph.'''
    # Initialize and covert variables appropriately.
    graph = set(graph)
    ordering = []
    candidates = list({edge[0] for edge in graph} - {edge[1] for edge in graph})

    # Get the topological ordering.
    while len(candidates) != 0:
        # Add the next candidate to the ordering.
        ordering.append(candidates[0])

        # Remove outgoing edges and store outgoing nodes.
        temp_nodes = []
        for edge in filter(lambda e: e[0] == candidates[0], graph):
            graph.remove(edge)
            temp_nodes.append(edge[1])

        # Add outgoing nodes to candidates list if it has no other incoming edges.
        for node in temp_nodes:
            if node not in {edge[1] for edge in graph}:
                candidates.append(node)

        # Remove the current candidate.
        candidates = candidates[1:]

    return ordering

def LongestPathDAG(source, sink, graph):
    # get the topological ordering
    top_order = TopologicalOrder(graph.keys(), source)
    # remove the source node
    top_order = top_order[top_order.index(source)+1:top_order.index(sink)+1]
    print top_order

    # initialize the DAG structure to hold all max weight edges
    DAG = {node: -100 for node in {edge[0] for edge in graph.keys()} | {edge[1] for edge in graph.keys()}}
    DAG[0] = 0
    back_track = {node: None for node in top_order}
    # fill the nodes with max weight possible from that longest path possible
    for node in top_order:
        try:
            # filter out the edges with start node that matchs with node
            filtered = filter(lambda e: e[1] == node, graph.keys())
            # add to every node the edge weight of its incoming edge
            mapped = map(lambda e: [DAG[e[0]] + graph[e], e[0]], filtered)
            # pick the node with max weight edge and assign it to that node
            DAG[node], back_track[node] = max(mapped, key = lambda p: p[0])
        except ValueError:
            pass

    DAG_path = [sink]
    while DAG_path[0] != source:
        DAG_path = [back_track[DAG_path[0]]] + DAG_path
    return DAG_path, DAG[sink]

lines = ReadFromFile(READ_PATH)

source = int(lines[0])
sink = int(lines[1])
g = FormGraph(lines[2:])

path, length = LongestPathDAG(source,sink,g)

f = open(WRITE_PATH, "w")
f.write(str(length) + "\n")
token = ""

for item in path:
    if item == path[-1]:
        token = token + str(item)
    else:
        token = token + str(item) + "->"
f.write(token)
f.close()