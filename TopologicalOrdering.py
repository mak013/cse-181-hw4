from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5n.txt"
WRITE_PATH = "Answers/rosalind_ba5n_ans.txt"

def TopologicalOrder(graph):

    graph = set(graph)
    ordering = []
    choices = list({edge[0] for edge in graph} - {edge[1] for edge in graph})
    choices = sorted(choices)

    # Get the topological ordering.
    while len(choices) != 0:
        # Add the next choice to the ordering.
        ordering.append(choices[0])

        # Remove outgoing edges and store outgoing nodes.
        temp_nodes = []
        for edge in filter(lambda e: e[0] == choices[0], graph):
            graph.remove(edge)
            temp_nodes.append(edge[1])

        # Add outgoing nodes to choices list if it has no other incoming edges.
        for node in temp_nodes:
            if node not in {edge[1] for edge in graph}:
                choices.append(node)

        # Remove the current choice.
        choices = choices[1:]

    return ordering

lines = ReadFromFile(READ_PATH)
graph = []
for line in lines:
	a = line.split(" -> ")[0]
	if "," in line.split("->")[1]:
		for b in line.split(" -> ")[1].split(","):
			graph.append((int(a),int(b)))
	else:
		graph.append((int(a), int(line.split(" -> ")[1])))

top_order = TopologicalOrder(graph)

f = open(WRITE_PATH, "w")
for item in top_order:
	if item == top_order[-1]:
		f.write(str(item))
	else:
		f.write(str(item)+", ")
f.close()