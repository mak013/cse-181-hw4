from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba5e.txt"
WRITE_PATH = "Answers/rosalind_ba5e_ans.txt"

def ScoringMatrix():
	lines = ReadFromFile("Data/BLOSUM63.txt")
	variables = lines[0].split()
	values = [line.split()[1:] for line in lines[1:]]

	scoring_matrix = {}
	for x in variables:
		for y in variables:
			scoring_matrix[(x,y)] = int(values[variables.index(x)][variables.index(y)])
	return scoring_matrix

def InitialMatrix(v,w):
	return [[0 for j in xrange(w)] for i in xrange(v)]

def GlobalAlignment(v, w, sigma):
	# initialize an empty matrix
	matrix = InitialMatrix(len(v)+1, len(w)+1)
	back_track = InitialMatrix(len(v)+1,len(w)+1)
	# intialize the beginning of the matrix with del penalties 
	# as initial values of the matrix
	for i in xrange(len(v)+1):
		matrix[i][0] = -i*sigma
	for j in xrange(len(w)+1):
		matrix[0][j] = -j*sigma
	# fill in the matrix with the scores
	scoring_matrix = ScoringMatrix()
	for i in xrange(1,len(v) + 1):
		for j in xrange(1,len(w)+1):
			# down, right, match
			# down if deletion, right if insert, and match is match
			scores = [matrix[i-1][j] - sigma, matrix[i][j-1] - sigma, matrix[i-1][j-1] + scoring_matrix[v[i-1],w[j-1]]]
			matrix[i][j] = max(scores)
			back_track[i][j] = scores.index(matrix[i][j])

	# Short function to handle insertions and indeletions
	# it's an insertions if dash in top string
	# it's an deletion if dash in bottom string
	indel_insert = lambda e,i: e[:i] + "-" + e[i:]


	max_score = matrix[len(v)][len(w)]
	i,j = len(v), len(w)
	v_aligned, w_aligned = v,w

	# back track to where the alignment begins 
	# to give us the back score
	while i*j != 0:
		if back_track[i][j] == 0:
			i -= 1
			w_aligned = indel_insert(w_aligned, j)
		elif back_track[i][j] == 1:
			j -= 1
			v_aligned = indel_insert(v_aligned, i)
		else:
			i -= 1
			j -= 1

	# either one of the words is going to be shorter than other
	# fill the shorter one in front with dashes
	for repeat in xrange(i):
		w_aligned = indel_insert(w_aligned, 0)
	for repeat in xrange(j):
		v_aligned = indel_insert(v_aligned, 0)

	return max_score, v_aligned, w_aligned

lines = ReadFromFile(READ_PATH)
v = lines[0]
w = lines[1]

score, v, w = GlobalAlignment(v,w,5)

f = open(WRITE_PATH, "w")
f.write(str(score)+"\n")
f.write(v+"\n")
f.write(w)
f.close()